/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {AppRegistry}from 'react-native';
import {createStore, applyMiddleware, combineReducers} from 'redux';
import createLogger from 'redux-logger';
import thunk from 'redux-thunk';

import {authReducer} from './src/auth';
import {eventReducer} from './src/event';
import {Router} from './src/Router'

const rootReducer = combineReducers({auth: authReducer,event:eventReducer});
const store = createStore(rootReducer, applyMiddleware(thunk, createLogger({colors: {}})));

export default class eventReactNative extends Component {
  render() {
    return (
      <Router store={store}/>
  )
  }
}
AppRegistry.registerComponent('eventReactNative', () => eventReactNative);
