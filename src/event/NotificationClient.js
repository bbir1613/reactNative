// const io = require('socket.io-client/socket.io');
const io = require('socket.io-client/dist/socket.io');
import {apiUrl} from '../core/api';
import {getLogger} from '../core/utils';
import {eventCreated, eventUpdated, eventDeleted} from './service';

window.navigator.userAgent = 'ReactNative';

const log = getLogger('NotificationClient');

const EVENT_CREATED = 'event/created';
const EVENT_UPDATED = 'event/updated';
const EVENT_DELETED = 'event/deleted';

export class NotificationClient {
    constructor(store) {
        this.store = store;
    }

    connect() {
        log(`connect...`);
        const store = this.store;
        const auth = store.getState().auth;
        this.socket = io(apiUrl, {transports: ['websocket']});
        const socket = this.socket;
        socket.on('connect', () => {
            log('connected');
            socket
                .emit('authenticate', {token: auth.token})
                .on('authenticated', () => log(`authenticated`))
                .on('unauthorized', (msg) => log(`unauthorized: ${JSON.stringify(msg.data)}`))
        });
        socket.on(EVENT_CREATED, (note) => {
            log(EVENT_CREATED);
            store.dispatch(eventCreated(note));
        });
        socket.on(EVENT_UPDATED, (note) => {
            log(EVENT_UPDATED);
            store.dispatch(eventUpdated(note))
        });
        socket.on(EVENT_DELETED, (note) => {
            log(EVENT_DELETED);
            store.dispatch(eventDeleted(note))
        });
    };

    disconnect() {
        log(`disconnect`);
        this.socket.disconnect();
    }
}