import React, {Component} from 'react';
import {Text, View, TextInput, ActivityIndicator} from 'react-native';
import {registerRightAction, issueToText, getLogger} from '../core/utils';
import {cancelSaveEvent, deleteEvent} from './service';
import styles from '../core/styles';

const log = getLogger('EventDelete');
const EVENT_DELETE_ROUTE = 'event/delete';

export class EventDelete extends Component {

    static get routeName() {
        return EVENT_DELETE_ROUTE;
    }

    static get route() {
        return {name: EVENT_DELETE_ROUTE, title: 'Event Delete', rightText: 'Delete'};
    }

    constructor(props) {
        log('constructor');
        super(props);
        const nav = this.props.navigator;
        this.navigator = nav;
        const currentRoutes = nav.getCurrentRoutes();
        const currentRoute = currentRoutes[currentRoutes.length - 1];
        if (currentRoute.data) {
            this.state = {event: {...currentRoute.data}, isSaving: false};
        } else {
            this.state = {event: {text: ''}, isSaving: false};
        }
        registerRightAction(nav, this.onSave.bind(this));
        this.store = this.props.store;
    }

    render() {
        log('render');
        const state = this.state;
        let message = issueToText(state.issue);
        return (
            <View style={styles.content}>
                { state.isSaving &&
                <ActivityIndicator animating={true} style={styles.activityIndicator} size="large"/>
                }
                <Text>Are you sure you want to delete : {state.event.text }</Text>
                {message && <Text>{message}</Text>}
            </View>
        );
    }

    componentDidMount() {
        log('componentDidMount');
        this._isMounted = true;
        const store = this.props.store;
        this.unsubscribe = store.subscribe(() => {
            log('setState');
            const state = this.state;
            const eventState = store.getState().event;
            this.setState({...state, issue: eventState.issue});
        });
    }

    componentWillUnmount() {
        log('componentWillUnmount');
        this._isMounted = false;
        this.unsubscribe();
        if (this.state.isLoading) {
            this.store.dispatch(cancelSaveEvent());
        }
    }

    onSave() {
        log('onSave');
        this.store.dispatch(deleteEvent(this.state.event)).then(() => {
            log('onDeleteSaved');
            if (!this.state.issue) {
                this.navigator.pop();
                this.navigator.pop();
            }
        });
    }
}