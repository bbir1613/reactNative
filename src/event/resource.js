import {getLogger, ResourceError} from '../core/utils';
import {authHeaders} from '../core/api';
import {apiUrl} from '../core/api';
const log = getLogger('event/resource');

export const search = async(server, token) => {
    // const url = `${serverUrl}/api/event`;
    const url = apiUrl;
    log(`GET ${url}`);
    let ok;
    let json = await fetch(url + '/event', {method: 'GET', headers: authHeaders(token)})
        .then(res => {
            ok = res.ok;
            return res.json();
        });
    return interpretResult('GET', url, ok, json);
};

export const save = async(server, token, event) => {
    const body = JSON.stringify(event);
    const url = event._id ? `${apiUrl}/event/${event._id}` : `${apiUrl}/event`;
    const method = event._id ? 'PUT' : 'POST';
    log(`${method} ${url}`);
    let ok;
    return fetch(url, {method, headers: authHeaders(token), body})
        .then(res => {
            ok = res.ok;
            return res.json();
        });
};

export const remove = async(server, token, event) => {
    // const body = JSON.stringify({});
    const url = `${apiUrl}/event/${event._id}`;
    const method = 'DELETE';
    log(`${method} ${url}`);
    let ok;
    let json = await fetch(url, {method, headers: authHeaders(token)})
        .then(res => {
            console.log(' it deletes');
            ok = res.ok;
            return event;
        });
    return interpretResult(method, url, ok, json);
};

function interpretResult(method, url, ok, json) {
    if (ok) {
        // log(`${method} ${url} succeeded`);
        return json;
    } else {
        // log(`${method} ${url} failed`);
        throw new ResourceError('Fetch failed', json.issue);
    }
}