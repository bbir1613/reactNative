import {action, getLogger, errorPayload} from '../core/utils';
import {search, save, remove} from './resource';

const log = getLogger('note/service');

// Loading notes
const LOAD_EVENTS_STARTED = 'event/loadStarted';
const LOAD_EVENTS = 'event/loadSucceeded';
const LOAD_EVENTS_FAILED = 'event/loadFailed';
const CANCEL_LOAD_EVENTS = 'event/cancelLoad';

// Saving notes
const SAVE_EVENT_STARTED = 'event/saveStarted';
const SAVE_EVENT_SUCCEEDED = 'event/saveSucceeded';
const SAVE_EVENT_FAILED = 'event/saveFailed';
const CANCEL_SAVE_EVENT = 'event/cancelSave';

// Saving notes
const DELETE_EVENT_STARTED = 'event/deleteStarted';
const DELETE_EVENT_SUCCEEDED = 'event/deleteSucceeded';
const DELETE_EVENT_FAILED = 'event/deleteFailed';
const CANCEL_DELETE_EVENT = 'event/cancelDelete';

// Note notifications
const EVENT_DELETED = 'event/deleted';

export const loadEvents = () => async(dispatch, getState) => {
    log(`loadEvents...`);
    const state = getState();
    const eventState = state.event;
    try {
        dispatch(action(LOAD_EVENTS_STARTED));
        const notes = await search(state.auth.server, state.auth.token);
        log(`loadNotes succeeded`);
        if (!eventState.isLoadingCancelled) {
            dispatch(action(LOAD_EVENTS, notes));
        }
    } catch (err) {
        log(`loadNotes failed`);
        if (!eventState.isLoadingCancelled) {
            dispatch(action(LOAD_EVENTS_FAILED, errorPayload(err)));
        }
    }
};

export const cancelLoadEvents = () => action(CANCEL_LOAD_EVENTS);

export const saveEvent = (event) => async(dispatch, getState) => {
    log(`saveNote...`);
    const state = getState();
    const eventState = state.event;
    try {
        dispatch(action(SAVE_EVENT_STARTED));
        const savedNote = await save(state.auth.server, state.auth.token, event);
        log(`saveNote succeeded`);
        if (!eventState.isSavingCancelled) {
            dispatch(action(SAVE_EVENT_SUCCEEDED, savedNote));
        }
    } catch (err) {
        log(`saveNote failed`);
        if (!eventState.isSavingCancelled) {
            dispatch(action(SAVE_EVENT_FAILED, errorPayload(err)));
        }
    }
};

export const deleteEvent = (event) => async(dispatch, getState) => {
    log("delete event ...");
    const state = getState();
    const eventState = state.event;
    try {
        dispatch(action(DELETE_EVENT_STARTED));
        const savedNote = await remove(state.auth.server, state.auth.token, event);
        log(`remove succeeded`);
        if (!eventState.isSavingCancelled) {
            dispatch(action(DELETE_EVENT_SUCCEEDED, savedNote));
        }
    } catch (err) {
        log(`saveNote failed`);
        if (!eventState.isSavingCancelled) {
            dispatch(action(DELETE_EVENT_FAILED, errorPayload(err)));
        }
    }
};

export const cancelSaveEvent = () => action(CANCEL_SAVE_EVENT);

export const eventCreated = (createdNote) => action(SAVE_EVENT_SUCCEEDED, createdNote);
export const eventUpdated = (updatedNote) => action(SAVE_EVENT_SUCCEEDED, updatedNote);
export const eventDeleted = (deletedNote) => action(EVENT_DELETED, deletedNote);

export const eventReducer = (state = {items: [], isLoading: false, isSaving: false}, action) => { //newState (new object)
    let items, index;
    switch (action.type) {
        // Loading
        case LOAD_EVENTS_STARTED:
            return {...state, isLoading: true, isLoadingCancelled: false, issue: null};
        case LOAD_EVENTS:
            return {...state, items: action.payload, isLoading: false};
        case LOAD_EVENTS_FAILED:
            return {...state, issue: action.payload.issue, isLoading: false};
        case CANCEL_LOAD_EVENTS:
            return {...state, isLoading: false, isLoadingCancelled: true};
        // Saving
        case SAVE_EVENT_STARTED:
            return {...state, isSaving: true, isSavingCancelled: false, issue: null};
        case SAVE_EVENT_SUCCEEDED:
            items = [...state.items];
            index = items.findIndex((i) => i._id == action.payload._id);
            if (index != -1) {
                items.splice(index, 1, action.payload);
            } else {
                items.push(action.payload);
            }
            return {...state, items, isSaving: false};
        case SAVE_EVENT_FAILED:
            return {...state, issue: action.payload.issue, isSaving: false};
        case CANCEL_SAVE_EVENT:
            return {...state, isSaving: false, isSavingCancelled: true};
        // Delete
        case DELETE_EVENT_STARTED:
            return {...state, isSaving: true, isSavingCancelled: false, issue: null};
            break;
        case DELETE_EVENT_SUCCEEDED:
            items = [...state.items].filter((i) => i._id !== action.payload._id);
            return {...state, items, isSaving: false};
            break;
        case DELETE_EVENT_FAILED:
            return {...state, isSaving: true, isSavingCancelled: false, issue: action.payload.issue};
            break;
        case CANCEL_DELETE_EVENT:
            return {...state, isSaving: true, isSavingCancelled: true};
            break;
        // Notifications
        case EVENT_DELETED:
            items = [...state.items];
            const deletedNote = action.payload;
            index = state.items.findIndex((note) => note._id == deletedNote._id);
            if (index != -1) {
                items.splice(index, 1);
                return {...state, items};
            }
            return state;
        default:
            return state;
    }
};