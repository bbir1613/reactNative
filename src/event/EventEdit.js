import React, {Component} from 'react';
import {Text, View, TextInput, ActivityIndicator, TouchableHighlight} from 'react-native';
import {registerRightAction, issueToText, getLogger} from '../core/utils';
import {saveEvent, cancelSaveEvent} from './service';
import {EventDelete} from './DeleteEvent'
import styles from '../core/styles';

const log = getLogger('EventEdit');
const EVENT_EDIT_ROUTE = 'event/edit';
export class EventEdit extends Component {

    static get routeName() {
        return EVENT_EDIT_ROUTE;
    }

    static get route() {
        return {name: EVENT_EDIT_ROUTE, title: 'Event New', rightText: 'Save'};
    }

    constructor(props) {
        log('constructor');
        super(props);
        const nav = this.props.navigator;
        this.navigator = nav;
        const currentRoutes = nav.getCurrentRoutes();
        const currentRoute = currentRoutes[currentRoutes.length - 1];
        if (currentRoute.data) {
            this.state = {event: {...currentRoute.data}, isSaving: false};
        } else {
            this.state = {event: {text: ''}, isSaving: false};
        }
        registerRightAction(nav, this.onSave.bind(this));
        this.store = this.props.store;
    }

    render() {
        log('render');
        const state = this.state;
        let message = issueToText(state.issue);
        return (
            <View style={styles.content}>
                { state.isSaving &&
                <ActivityIndicator animating={true} style={styles.activityIndicator} size="large"/>
                }
                <Text>Text</Text>
                <TextInput value={state.event.text} onChangeText={(text) => this.updateEventText(text)}/>
                {message && <Text>{message}</Text>}
                {this.state.event._id ?
                    <TouchableHighlight onPress={() => this.onDeleteEventPress(this.state.event)}>
                        <View>
                            <Text style={styles.button}>{'Delete'}</Text>
                        </View>
                    </TouchableHighlight> : <Text>{''}</Text>
                }
            </View>
        );
    }

    componentDidMount() {
        log('componentDidMount');
        this._isMounted = true;
        const store = this.props.store;
        this.unsubscribe = store.subscribe(() => {
            log('setState');
            const state = this.state;
            const eventState = store.getState().event;
            this.setState({...state, issue: eventState.issue});
        });
    }

    componentWillUnmount() {
        log('componentWillUnmount');
        this._isMounted = false;
        this.unsubscribe();
        if (this.state.isLoading) {
            this.store.dispatch(cancelSaveEvent());
        }
    }


    updateEventText(text) {
        let newState = {...this.state};
        newState.event.text = text;
        this.setState(newState);
    }

    onSave() {
        log('onSave');
        this.store.dispatch(saveEvent(this.state.event)).then(() => {
            log('onEventSaved');
            if (!this.state.issue) {
                this.navigator.pop();
            }
        });
    }

    onDeleteEventPress(event) {
        log('onLongEventPress');
        this.props.navigator.push({...EventDelete.route, title: 'Event Delete', rightText: 'Delete', data: event});
    }
}