import React, {Component} from 'react';
import {ListView, Text, View, StatusBar, ActivityIndicator} from 'react-native';
import {EventEdit} from './EventEdit';
import {EventDelete} from './DeleteEvent';
import {EventView} from './EventView';
import {loadEvents, cancelLoadEvents} from './service';
import {registerRightAction, getLogger, issueToText} from '../core/utils';
import styles from '../core/styles';

const log = getLogger('EventList');
const EVENT_LIST_ROUTE = 'event/list';

export class EventList extends Component {
    static get routeName() {
        return EVENT_LIST_ROUTE;
    }

    static get route() {
        return {name: EVENT_LIST_ROUTE, title: 'Event List', rightText: 'New'};
    }

    store;

    constructor(props) {
        super(props);
        log('constructor');
        this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1._id !== r2._id});
        const eventState = this.props.store.getState().event;
        this.state = {isLoading: eventState.isLoading, dataSource: this.ds.cloneWithRows(eventState.items)};
        registerRightAction(this.props.navigator, this.onNewNote.bind(this));
        this.store = this.props.store;
    }

    componentWillMount() {
        log('componentWillMount');
        // this.updateState();
        // registerRightAction(this.navigator, this.onLogin.bind(this));
        // registerRightAction(this.navigator, this.onLogin.bind(this));
    }


    render() {
        log('render');
        let message = issueToText(this.state.issue);
        return (
            <View style={styles.content}>
                { this.state.isLoading &&
                <ActivityIndicator animating={true} style={styles.activityIndicator} size="large"/>
                }
                {message && <Text>{message}</Text>}
                <ListView
                    dataSource={this.state.dataSource}
                    enableEmptySections={true}
                    renderRow={
                        event => (<EventView
                        event={event} onPress={(event) => this.onEventPress(event)}
                        />)
                    }/>
            </View>
        );
    }

    onNewNote() {
        log('onNewNote');
        this.props.navigator.push({...EventEdit.route});
    }

    onEventPress(event) {
        log('onEventPress');
        this.props.navigator.push({...EventEdit.route, title: 'Event Edit', rightText: 'Update', data: event});
    }

    componentDidMount() {
        log('componentDidMount');
        this._isMounted = true;
        const store = this.store;
        this.unsubscribe = store.subscribe(() => {
            log('setState');
            const eventState = store.getState().event;
            this.setState({dataSource: this.ds.cloneWithRows(eventState.items), isLoading: eventState.isLoading});
        });
        store.dispatch(loadEvents());
    }

    componentWillUnmount() {
        log('componentWillUnmount');
        this._isMounted = false;
        this.unsubscribe();
        if (this.state.isLoading) {
            this.store.dispatch(cancelLoadEvents());
        }
    }
}
