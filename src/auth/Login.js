import React, {Component} from 'react';
import {Text, View, TextInput, StyleSheet, ActivityIndicator, Vibration, Animated,Easing} from 'react-native';
import {getLogger, registerRightAction} from '../core/utils';
import {login, loadUserAndServer} from './service';
import styles from '../core/styles';

const log = getLogger('auth/Login');

const LOGIN_ROUTE = 'auth/login';

export class Login extends Component {
    static get routeName() {
        return LOGIN_ROUTE;
    }

    static get route() {
        return {name: LOGIN_ROUTE, title: 'Authentication', rightText: 'Login'};
    }

    constructor(props) {
        super(props);
        this.state = {username: '', password: ''};
        this.store = this.props.store;
        this.navigator = this.props.navigator;
        this.animatedValue = new Animated.Value(0)
        log('constructor');
    }

    componentWillMount() {
        log('componentWillMount');
        // this.updateState();
        // registerRightAction(this.navigator, this.onLogin.bind(this));
        registerRightAction(this.navigator, this.onLogin.bind(this));
    }

    render() {
        log('render');
        const movingMargin = this.animatedValue.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [0, 300, 0]
        });
        return (
            <View style={styles.content}>
                <Text>Username : </Text>
                <TextInput
                    onChange={(text) => this.setState({...this.state,username:text.nativeEvent.text})}
                />
                <Text>Password : </Text>
                <TextInput
                    secureTextEntry={true}
                    onChange={(text) => this.setState({...this.state,password:text.nativeEvent.text})}
                />
                <Animated.View
                    style={{marginLeft: movingMargin,marginTop: 10,height: 30,width: 40,backgroundColor: 'orange'}}
                />
            </View>
        )
    }

    animate () {
        this.animatedValue.setValue(0);
        Animated.timing(
            this.animatedValue,
            {
                toValue: 1,
                duration: 2000,
                easing: Easing.linear
            }
        ).start(() => this.animate())
    }

    componentDidMount() {
        log(`componentDidMount`);
        this.unsubscribe = this.store.subscribe(() => this.updateState());
        this.animate();
    }

    componentWillUnmount() {
        log(`componentWillUnmount`);
        this.unsubscribe();
    }

    updateState() {
        log(`updateState`);
        this.setState({...this.state, auth: this.store.getState().auth});
    }

    onLogin() {
        log('onLogin');
        console.log(' onLogin state', this.state);
        if (this.state.username.length === 0 || this.state.password.length === 0) {
            Vibration.vibrate([0, 400, 200, 100], false);//pattern ,repeat
            return;
        }
        this.store.dispatch(login({username: this.state.username, password: this.state.password})).then(() => {
            if (this.state.auth && this.state.auth.token) {
                console.log("logged in ");
                this.props.onAuthSucceeded();
            }
        })
    }
}