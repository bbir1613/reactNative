export const apiUrl = 'http://192.168.43.41:3000/api'; //cs

export const headers = {'Accept': 'application/json', 'Content-Type': 'application/json'};
export const authHeaders = (token) => ({...headers, 'Authorization': `Bearer ${token}`});
